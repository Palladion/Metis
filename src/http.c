#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/utils.h"
#include "../headers/http.h"
#include "../headers/file-security.h"
#include "../headers/images.h"

char html200[] = "HTTP/1.1 200 OK\nServer: Metis/1.0.0\nContent-Type: text/html; charset=utf-8\n\n";
char html404[] = "HTTP/1.1 404 OK\n\n<h1>404</h1>\n";
char htmlbad[] = "HTTP/1.1 404 OK\nContent-Type: text/html; charset=utf-8\n\n<h1>FAILED TO PARSE</h1>";
char htmlallow[] = "HTTP/1.1 404 OK\nContent-Type: text/html; charset=utf-8\n\n<h1>FILE NOT ALLOWED TO SHARE</h1>";
char typehtml[] = "Content-Type: text/html; charset=utf-8\n";
char typepng[] = "HTTP/1.1 200 OK\nServer: Metis/1.0.0\nContent-Type: image/png\nContent-Encoding: gzip\n\n";
char root[] = "index.html";

char * get_filename(char * request) {
    get_firstline(request); 
    char * filename = malloc(strlen(request));
    memset(filename,0,strlen(request));
    for (int x = 0; x < strlen(request); x++) {
        if (request[4+x] == ' ') { request[x] = 0; }
        else { filename[x] = request[4+x]; }
    }
    if (strlen(filename) != 1) { if (filename[0] == '/') {
            for (int x = 0; x < strlen(filename); x++) {
                filename[x] = filename[x+1];
            }
    }} else {
        free(filename); filename = malloc(strlen(root)+5);
        memset(filename,0,strlen(root)+5);
        memcpy(filename,root,strlen(root));
    }
    return filename;
}

char * parse_request(char * request, int * write) { 
    char * output;
    char * filename = get_filename(request);
    printf("[Logger] Request file %s\n",filename);
    FILE * file = open_file(filename);
    if (file == NULL) {
        char * output = malloc(strlen(html404));
        memset(output,0,strlen(html404));
        memcpy(output,html404,strlen(html404));
        *write = strlen(html404);
        return output;
    }
    if (is_PNG(file) != 0) {
        int filesize = is_PNG(file);
        char * filedata = malloc(filesize+10); 
        char * compress = malloc(filesize+100);
        char * output = malloc(strlen(typepng)+filesize+100);
        memset(filedata,0,filesize+10); memset(compress,0,filesize+100); 
        memset(output,0,strlen(typepng)+filesize+100);
        fread(filedata,1,filesize,file);
        int s = compressToGzip(filedata,filesize,compress,filesize+100);
        int written = 0;
        for (int x = 0; x < strlen(typepng); x++) { output[x] = typepng[x]; written++; }
        for (int x = 0; x < s; x++) { output[written+x] = compress[x]; }
        written = written + s;
        *write = written;
        fclose(file); free(filedata); free(compress);
        return output;
    }
    char * filedata = read_file(file); int written = strlen(html200);
    output = malloc(strlen(html200)+strlen(filedata)+10);
    memset(output,0,strlen(html200)+strlen(filedata)+10);
    memcpy(output,html200,strlen(html200));
    for (int x = 0; x < strlen(filedata); x++) { output[written] = filedata[x]; written++; }
    output[written-1] = 0x0d; output[written] = 0x0a; written++;  
    *write = written;
    fclose(file); free(filedata);
    return output;
    }

