#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include "../headers/http.h"
#include "../headers/sock.h"

int start_server(int port) {
    struct sockaddr_in serv;
    socklen_t socksize = sizeof(struct sockaddr_in);
    memset(&serv,0,sizeof(serv));
    serv.sin_family = AF_INET; serv.sin_addr.s_addr = htonl(INADDR_ANY); serv.sin_port = htons(port);
    int mysocket = socket(AF_INET,SOCK_STREAM,0);
    int on = 1; setsockopt(mysocket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
    (void)bind(mysocket,(struct sockaddr *)&serv,sizeof(struct sockaddr));
    listen(mysocket,1);
    return mysocket;
}

void answer_client(int mysocket,char * request) {
    struct sockaddr_in dest; socklen_t socksize = sizeof(struct sockaddr_in);
    int consocket = 1; int len = 0;
    char * response;
    struct timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0; 
    while (consocket) {
    consocket = accept(mysocket,(struct sockaddr *)&dest, &socksize);
    setsockopt(consocket,SOL_SOCKET,SO_RCVTIMEO, &timeout,sizeof(timeout));
    len = recv(consocket,request,999,0); request[len] = '\0';
    if (len <= 0) { printf("Error!\n"); } else {
        printf("[Metis] Request recieved!\n"); 
        int w = 0; char * output = parse_request(request,&w);
        printf("[Debug] (%d bytes)\n",w);
        send(consocket,output,w,0);
        free(output);
    } 
    close(consocket);
    printf("[Debug] Connection finished\n");
    }
}
