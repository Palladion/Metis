#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/file-security.h"
#include "../headers/utils.h"

int allowed_file(char * filename) {
    FILE *f = fopen("srv/accept","r");
    ssize_t n; char * line = NULL; size_t len = 0; int re = 1;
    if (f == NULL) { return 0; }
    while ((n = getline(&line,&len,f)) != -1) {
        remove_linebreak(line);  
        if (strcmp(line,filename) == 0) {
            re = 0;
        }
    }
    fclose(f); free(line); return re;
}

FILE * open_file(char * filename) {
    int check = allowed_file(filename); 
    if (check == 0) {
        char * path = malloc(6+strlen(filename)); 
        memset(path,0,6+strlen(filename));
        sprintf(path,"srv/%s",filename);
        FILE * a = fopen(path,"r"); free(path);
        return a;
    } else {
        return NULL;
    }
}

char * read_file(FILE * file) {
    int size; fseek(file,0L,SEEK_END); size = ftell(file); fseek(file,0L,SEEK_SET);
    char * out = malloc(size+5); 
    memset(out,0,size+5);
    fread(out,1,size,file);
    return out;
}
