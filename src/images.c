#include <zlib.h>
#include <stdio.h>
#include <string.h>
#include "../headers/images.h"

int is_PNG(FILE * f) {
    fseek(f,0L,SEEK_SET);
    char magic[5]; fread(magic,1,4,f);
    fseek(f,0L,SEEK_SET);
    if (magic[1] == 'P' && magic[2] == 'N' && magic[3] == 'G') {
       int size; fseek(f,0L,SEEK_END); size = ftell(f); fseek(f,0L,SEEK_SET);
      return size; 
    }
    return 0;
}

int compressToGzip(const char* input, int inputSize, char* output, int outputSize) {
    z_stream zs;
    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = Z_NULL;
    zs.avail_in = (uInt)inputSize;
    zs.next_in = (Bytef *)input;
    zs.avail_out = (uInt)outputSize;
    zs.next_out = (Bytef *)output;
    deflateInit2(&zs, Z_DEFAULT_COMPRESSION, Z_DEFLATED, 15 | 16, 8, Z_DEFAULT_STRATEGY);
    deflate(&zs, Z_FINISH);
    deflateEnd(&zs);
    return zs.total_out;
}
