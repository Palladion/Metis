#include <stdio.h>
#include <stdlib.h>
#include "headers/sock.h"
#include <string.h>

int main() {
    printf("[Metis] Starting up!\n");
    int socket = start_server(2000);
    char * request = malloc(1000);
    while (1) {
        answer_client(socket,request);
        memset(request,0,1000);
    }
}
